package com.slotmachineproject.slots

import android.graphics.Point

data class WinningRaw(val startY: Int, val directions: List<Direction>){
    val coordinates: List<Point>

    init {
        val prev = Point(0,startY)
        coordinates = List(directions.size + 1){
            if(it != 0){
                prev.x++
                prev.y += directions[it - 1].delta()
            }
            Point(prev.x, prev.y)
        }
    }

    fun isWinning(table: Table) =
        (checkEqual(table, 0,4) && checkEqual(table,1,2,3)) ||
        (checkEqual(table, 0,2,4) && checkEqual(table,1,3)) ||
        (checkEqual(table, 0,1,2,3)) ||
        (checkEqual(table, 1,2,3,4))

    private fun checkEqual(table: Table, vararg indexes: Int): Boolean{
        if(indexes.size == 1)return true
        var prev = indexes.first()
        for(i in 1 until indexes.size){
            val index = indexes[i]
            if(table.get(coordinates[prev]) != table.get(coordinates[index]))return false
            prev = index
        }
        return true
    }
}