package com.slotmachineproject.slots

import android.graphics.Color
import com.slotmachineproject.R

object Packs{
    val Casino = Pack(listOf(
        R.drawable.pack_casino_1,
        R.drawable.pack_casino_2,
        R.drawable.pack_casino_3,
        R.drawable.pack_casino_4,
        R.drawable.pack_casino_5,
        R.drawable.pack_casino_6,
        R.drawable.pack_casino_7,
        R.drawable.pack_casino_8,
        R.drawable.pack_casino_9,
    ),R.drawable.back_casino,R.color.light_red,R.color.dark_purple)
    val Pirates = Pack(listOf(
        R.drawable.pack_pirate_1,
        R.drawable.pack_pirate_2,
        R.drawable.pack_pirate_3,
        R.drawable.pack_pirate_4,
        R.drawable.pack_pirate_5,
        R.drawable.pack_pirate_6,
        R.drawable.pack_pirate_7,
        R.drawable.pack_pirate_8,
        R.drawable.pack_pirate_9,
    ),R.drawable.back_pirate,R.color.light_purple,R.color.dark_purple)
    val Egypt = Pack(listOf(
        R.drawable.pack_egypt_1,
        R.drawable.pack_egypt_2,
        R.drawable.pack_egypt_3,
        R.drawable.pack_egypt_4,
        R.drawable.pack_egypt_5,
        R.drawable.pack_egypt_6,
        R.drawable.pack_egypt_7,
        R.drawable.pack_egypt_8,
        R.drawable.pack_egypt_9,
    ),R.drawable.back_egypt,R.color.light_blue, R.color.dark_purple)
}

data class Pack(val images: List<Int>, val fieldBackground: Int, val firstColor: Int, val secondColor: Int)