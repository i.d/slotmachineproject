package com.slotmachineproject.slots

import android.content.Context.MODE_PRIVATE
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.slotmachineproject.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import kotlin.math.max
import kotlin.random.Random

class SlotGameFragment : Fragment(R.layout.fragment_slot_game) {
    companion object {
        const val LINE_SHOW_DURATION = 500L
        const val SPIN_DURATION = 3000L
        const val SPIN_DURATION_INTERVAL = 400L
        private lateinit var pack: Pack

        fun getInstance(pack: Pack) = SlotGameFragment().also {
            this.pack = pack
        }

    }

    private val viewModel: SlotViewModel by viewModels()

    private lateinit var columns: List<SlotColumn>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(
            requireActivity().getSharedPreferences(Constants.SLOT_KEY, MODE_PRIVATE),
            pack
        )
    }

    override fun onPause() {
        super.onPause()
        viewModel.save(requireActivity().getSharedPreferences(Constants.SLOT_KEY, MODE_PRIVATE))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.setBackgroundResource(pack.fieldBackground)

        createColumns(view)

        setupSpin(view)

        setupBetLayout(view)
        setupWinLayout(view)
        setupMoney(view)
        setupMaxButton(view)
    }

    private fun setupMaxButton(view: View) {
        view.findViewById<Button>(R.id.btnMaxBet).setOnClickListener {
            viewModel.pressMax()
        }
    }

    private fun setupMoney(view: View) {
        val tvMoney = view.findViewById<TextView>(R.id.tvMoney)
        viewModel.moneyFlow.onEach {
            tvMoney.text = it.toString()
        }.launchIn(lifecycleScope)
    }

    private fun setupWinLayout(view: View) {
        val tvWin = view.findViewById<TextView>(R.id.tvWin)
        viewModel.winFlow.onEach {
            tvWin.text = it.toString()
        }.launchIn(lifecycleScope)
    }

    private fun setupSpin(view: View) {
        view.findViewById<Button>(R.id.btnSpin).setOnClickListener {
            if (!viewModel.spin()) {
                onSpinDisallowed()
            } else {
                onSpinAllowed(it)
            }
        }
    }

    private fun onSpinAllowed(button: View) {
        button.isClickable = false

        val maximum = SPIN_DURATION + SPIN_DURATION_INTERVAL * columns.size
        columns.forEachIndexed {index,it ->
            it.spin(SPIN_DURATION + index * SPIN_DURATION_INTERVAL)
        }
        val mp = MediaPlayer.create(requireContext(), R.raw.win_sound)
        mp.setOnCompletionListener {
            try{
                mp.prepare()
                mp.seekTo(0)
            }catch (e: Exception){}
        }
        lifecycleScope.launch {
            delay(maximum)
            var wasFirstWin = false
            viewModel.raws.forEach { winningRaw ->
                if (winningRaw.isWinning(viewModel.table)) {
                    if(!wasFirstWin){
                        wasFirstWin = true
                        mp.start()
                    }
                    winningRaw.coordinates.forEach { columns[it.x].isWinningImage(it.y, true) }

                    viewModel.addWin()
                    delay(LINE_SHOW_DURATION)

                    winningRaw.coordinates.forEach { columns[it.x].isWinningImage(it.y, false) }
                }
            }

            button.isClickable = true
        }
    }

    private fun onSpinDisallowed() {
        Toast.makeText(requireContext(), getString(R.string.not_enough_money), Toast.LENGTH_SHORT)
            .show()
    }

    private fun createColumns(view: View) {
        columns = listOf(
            SlotColumn(view.findViewById(R.id.frameLayout1), viewModel.table[0], 0),
            SlotColumn(view.findViewById(R.id.frameLayout2), viewModel.table[1], 1),
            SlotColumn(view.findViewById(R.id.frameLayout3), viewModel.table[2], 2),
            SlotColumn(view.findViewById(R.id.frameLayout4), viewModel.table[3], 3),
            SlotColumn(view.findViewById(R.id.frameLayout5), viewModel.table[4], 4),
        )

        columns.forEach { it.init() }
    }

    private fun setupBetLayout(view: View) {
        view.findViewById<ImageButton>(R.id.btnMinus).setOnClickListener {
            viewModel.pressMinus()
        }
        view.findViewById<ImageButton>(R.id.btnPlus).setOnClickListener {
            viewModel.pressPlus()
        }
        val tvTotalBet = view.findViewById<TextView>(R.id.tvTotalBet)
        viewModel.betFlow.onEach {
            tvTotalBet.text = it.toString()
        }.launchIn(lifecycleScope)
    }
}