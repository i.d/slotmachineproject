package com.slotmachineproject.slots

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.ViewModel
import com.slotmachineproject.slots.Direction.*
import kotlinx.coroutines.flow.MutableStateFlow

class SlotViewModel : ViewModel(){
    companion object{
        val betValues = listOf<Long>(1,5,10,25,50,100,500,1000,5000,10000)

        const val START_MULTIPLIER = 3L
        const val INCREASE_MULTIPLIER_BY = 4f/3
    }

    val raws: MutableList<WinningRaw> = mutableListOf()
    init {
        getWinRaws(1, 1, mutableListOf())
        getWinRaws(2, 2, mutableListOf())
        getWinRaws(3, 3, mutableListOf())
    }

    private fun getWinRaws(start: Int, curY: Int, directions: List<Direction>){
        if(directions.size == 4){
            raws.add(WinningRaw(start, directions))
            return
        }
        if(curY - 1 >= 1)getWinRaws(start, curY - 1, directions.plus(UP))
        getWinRaws(start, curY, directions.plus(FRONT))
        if(curY + 1 <= 3)getWinRaws(start, curY + 1, directions.plus(DOWN))
    }


    val betFlow: MutableStateFlow<Long> = MutableStateFlow(10)
    val winFlow: MutableStateFlow<Long> = MutableStateFlow(300)
    val moneyFlow: MutableStateFlow<Long> = MutableStateFlow(1000)
    lateinit var table: Table

    fun init(pref: SharedPreferences, pack: Pack){
        betIndex = pref.getInt(Constants.BET_KEY, 2)
        betFlow.value = betValues[betIndex]
        winFlow.value = pref.getLong(Constants.WIN_KEY,300)
        moneyFlow.value = pref.getLong(Constants.COINS_KEY,1000)

        table = Table(List(5){ Column(pack,4) })
    }

    fun save(pref: SharedPreferences){
        pref.edit {
            putInt(Constants.BET_KEY, betIndex)
            putLong(Constants.WIN_KEY, winFlow.value)
            putLong(Constants.COINS_KEY, moneyFlow.value)
        }
    }

    private fun changeBet(delta: Int){
        if(0 <= betIndex + delta && betIndex + delta < betValues.size){
            betIndex += delta
        }
        betFlow.value = betValues[betIndex]
    }

    private var currentBet: Long = 0
    private var multiplier: Long = 0

    fun spin(): Boolean{
        multiplier = START_MULTIPLIER
        if(betValues[betIndex] > moneyFlow.value)return false
        currentBet = betValues[betIndex]
        moneyFlow.value -= currentBet
        winFlow.value = 0
        return true
    }

    fun addWin(){
        moneyFlow.value += currentBet * multiplier
        winFlow.value += currentBet * multiplier

        multiplier = (multiplier * INCREASE_MULTIPLIER_BY).toLong()
    }

    private var betIndex = 2

    fun pressPlus() = changeBet(1)
    fun pressMinus() = changeBet(-1)
    fun pressMax(){
        betIndex = 0
        while(betIndex < betValues.size && betValues[betIndex] <= moneyFlow.value)betIndex++
        if(betIndex > 0)betIndex--
        changeBet(0)
    }
}