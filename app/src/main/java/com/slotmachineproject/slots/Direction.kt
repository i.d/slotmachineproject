package com.slotmachineproject.slots

enum class Direction {
    UP,FRONT,DOWN
}

fun Direction.delta() = when(this){
    Direction.UP -> -1
    Direction.FRONT -> 0
    Direction.DOWN -> 1
}