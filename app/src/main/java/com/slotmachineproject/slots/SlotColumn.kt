package com.slotmachineproject.slots

import android.animation.ValueAnimator
import android.graphics.drawable.GradientDrawable
import android.media.MediaPlayer
import android.media.SoundPool
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.updateLayoutParams
import com.slotmachineproject.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random

class SlotColumn(
    private val column: FrameLayout,
    private val columnData: Column,
    private val index: Int
) {
    companion object{
        const val MIN_SPIN_TIMES = 20
        const val MAX_SPIN_TIMES = 40
        val sp = SoundPool.Builder().setMaxStreams(5).build()
    }
    private lateinit var images: List<ImageView>
    private var poolIndex: Int = 0

    fun init() {
        poolIndex = sp.load(column.context, R.raw.stop_sound, 0)
        setBackground()
        createImages()
        resizeLayout()
    }

    private fun setBackground() {
        (column.background as GradientDrawable).setColor(
            ContextCompat.getColor(
                column.context,
                if (index % 2 == 0) columnData.pack.firstColor else columnData.pack.secondColor
            )
        )
    }

    private fun resizeLayout() {
        images.first().doOnPreDraw {
            column.updateLayoutParams {
                val ht: Int
                if (it.height * 3 > column.height) {
                    width = column.height / 3
                    ht = width
                } else {
                    height = it.height * 3
                    ht = height
                }
                images.forEachIndexed { index, it ->
                    it.translationY = ((index - 1) * ht).toFloat()
                }
            }
        }
    }

    private fun createImages() {
        images = List(columnData.size) {
            ImageView(column.context).apply {
                adjustViewBounds = true
                scaleType = ImageView.ScaleType.FIT_XY
                setImageResource(columnData.elements[it])
            }
        }
        images.forEach {
            column.addView(it, MATCH_PARENT, WRAP_CONTENT)
        }
    }

    private var prevFrom = 0f

    fun spin(duration: Long) {
        val height = images.first().height
        val delta: Int
        with(Random(System.nanoTime()).nextInt(MIN_SPIN_TIMES, MAX_SPIN_TIMES)) {
            delta = this * height
        }

        val from = prevFrom
        val to = from + delta.toFloat()

        prevFrom = to


        ValueAnimator.ofFloat(from, to).apply {
            var prev = from
            addUpdateListener {
                val value = it.animatedValue as Float
                translateAll(value, (value - prev), height)
                prev = value
            }
            doOnEnd {
                setShift()
                sp.play(poolIndex,.5f,.5f,0,0,1f)
            }
            this.duration = duration
            start()
        }
    }

    private fun translateAll(newValue: Float, delta: Float, height: Int) {
        val fullHeight = height * images.size
        for (index in (images.size - 1 downTo 0)) {
            val imageView = images[index]
            if (imageView.translationY + delta + height >= fullHeight)
                setNewImage(index)
            imageView.translationY =
                (newValue + index * height).mod(fullHeight.toFloat()) - height
        }
    }

    fun isWinningImage(id: Int, isWinning: Boolean) {
        val newId = (id + columnData.shift) % images.size
        images[newId].setBackgroundResource(if (isWinning) R.drawable.win_slot else android.R.color.transparent)
    }

    private fun setShift() {
        columnData.shift =
            images.indexOfFirst { it.translationY < 0 }
    }

    private fun setNewImage(index: Int) {
        columnData.set(index)
        images[index].setImageResource(columnData.elements[index])
    }
}