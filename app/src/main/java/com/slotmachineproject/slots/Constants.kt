package com.slotmachineproject.slots

object Constants {
    const val BET_KEY = "bet_flow"
    const val WIN_KEY = "win_flow"
    const val COINS_KEY = "coins"
    const val SLOT_KEY = "slotPreferences"
}