package com.slotmachineproject.slots

import android.graphics.Point

data class Table(val columns: List<Column>){
    fun get(point: Point) = this[point.x][point.y]
    operator fun get(index: Int) = columns[index]
}