package com.slotmachineproject.slots

import kotlin.random.Random

data class Column(var pack: Pack, val size: Int){
    var shift: Int = 0
    val elements: MutableList<Int> = MutableList(size){nextImage()}

    private val random
        get() = Random(System.nanoTime())

    private fun nextImage() = pack.images[random.nextInt(pack.images.size)]

    fun set(index: Int){
        elements[index] = nextImage()
    }

    operator fun get(index: Int) = elements[(index + shift) % elements.size]
}
